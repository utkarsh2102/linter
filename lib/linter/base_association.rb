# frozen_string_literal: true

module Linter
  class BaseAssociation
    def self.analyze(text)
      result = OpenStruct.new(trend: '')

      wordlists.dig('words').each do |key, words|
        word_count_key = "#{key}_word_counts".to_sym
        result[word_count_key] = {}
        words.each do |word|
          result.send(word_count_key).merge!(word_count(text, word))
        end
      end

      result.trend = calculate_trend(result)
      result
    end

    def self.word_count(text, word)
      if self::FULL_WORD
        regex = /\b#{word}\b/i
      else
        regex = /\b(#{word}\w*)\b/i
      end
      matches = text.scan(regex)
      return {} unless matches.any?

      # Use Enumerable#tally with Ruby 2.7
      matches
        .flatten
        .map(&:downcase)
        .group_by { |v| v }
        .transform_values(&:size)
        .to_h
    end
  end
end
